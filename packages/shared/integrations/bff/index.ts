import { DocumentData, ShortDocumentInfo } from '@shared/types/document';
import { UserData } from '@shared/types/user';

export type AuthorizePayload = {
    token: string;
};

export type AuthorizeResponse = {
    user: UserData;
    accessToken: string;
};

export type RefreshTokenResponse = {
    user: UserData;
    accessToken: string;
};

export type GetAllUsersResponse = {
    users: UserData[];
};

export type CreateDocumentPayload = {
    name: string;
};

export type CreateDocumentResponse = {
    doc: DocumentData;
};

export type GetDocumentPayload = {
    docId: string;
    key: string;
};

export type GetDocumentResponse = {
    doc: DocumentData;
};

export type GetDocumentsResponse = {
    docs: ShortDocumentInfo;
};
