export function omitUndefined<T>(val: T | undefined): T {
    if (val === undefined) throw new Error();

    return val;
}
