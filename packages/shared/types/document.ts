import { DeltaOperation } from './delta';

export type DocumentData = {
    documentSnapshot: string;
    name: string;
    docId: string;
    userId: string;
};

export type ShortDocumentInfo = {
    name: string;
    docId: string;
    userId: string;
};

export type DocumentAccessData = {
    key: string;
    docId: string;
};

export type DocumentState = DeltaOperation[];
