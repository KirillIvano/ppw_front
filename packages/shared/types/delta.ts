export interface OptionalAttributes {
    attributes?: Record<string, any> | undefined;
}
export type DeltaOperation = {
    insert?: any;
    delete?: number | undefined;
    retain?: number | undefined;
} & OptionalAttributes;
