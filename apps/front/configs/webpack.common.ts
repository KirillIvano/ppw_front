/// <reference types="webpack-dev-server" />

import { type Configuration } from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

import path from 'path';

export const SRC_PATH = path.resolve(__dirname, '../src');
export const DIST_PATH = path.resolve(__dirname, '../dist');

export const baseConfig: Configuration = {
    entry: {
        main: path.join(SRC_PATH, 'index.tsx'),
        read: path.join(SRC_PATH, 'apps', 'readonly', 'index.tsx'),
    },
    resolve: {
        alias: {
            vars: path.join(SRC_PATH, 'common'),
            '@front': SRC_PATH,
        },
        extensions: ['.tsx', '.ts', '.js', '.jsx'],
    },
    output: {
        path: DIST_PATH,
        publicPath: '/assets/',
        filename: '[name].js',
        clean: true,
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: path.join(DIST_PATH, 'index.html'),
            template: path.join(SRC_PATH, 'index.html'),
            chunks: ['main'],
        }),
    ],
    module: {
        rules: [
            {
                test: /.html$/,
                loader: 'html-loader',
            },
        ],
    },
};
