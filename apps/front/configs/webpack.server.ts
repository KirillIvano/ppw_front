/// <reference types="webpack-dev-server" />

import { Configuration, DefinePlugin } from 'webpack';
import externals from 'webpack-node-externals';

import path from 'path';
import { DIST_PATH, SRC_PATH } from './webpack.common';

const serverConfig: Configuration = {
    target: 'node',

    mode: 'development',
    optimization: { minimize: false },
    externalsPresets: { node: true },
    externals: [externals()],

    resolve: {
        alias: {
            vars: path.join(SRC_PATH, 'common'),
            '@front': SRC_PATH,
        },
        extensions: ['.tsx', '.ts', '.js', '.jsx'],
    },
    entry: path.join(SRC_PATH, './server/index.tsx'),
    output: {
        filename: 'server.js',
        path: DIST_PATH,
    },
    plugins: [
        // TODO move to shared confing
        new DefinePlugin({
            __SOCKET_SERVICE__: '"ws://localhost:1234"',
            __IMAGES_SERVICE__: '"http://localhost:3001"',
            __STATIC_IMAGES_SERVICE__: '"http://localhost:3001/image"',
            __YA_APP_ID__: '"14ea8bd7381c4e9f88e056600fb43477"',
            __SERVER_SERVICE__: '"http://127.0.0.1:5001"',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.s?css$/,
                exclude: /quill\.(core|bubble|snow)\.css$/,
                use: [
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                exportOnlyLocals: true,
                                localIdentName: '[hash:8]',
                            },
                        },
                    },
                    'sass-loader',
                ],
            },
            {
                test: /\.(jpeg|jpg|png)$/,
                type: 'asset/resource',
                generator: {
                    emit: false,
                },
            },
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'esbuild-loader',
                        options: {
                            loader: 'tsx',
                        },
                    },
                    {
                        loader: 'babel-loader',
                        options: {
                            plugins: [
                                [
                                    '@babel/plugin-syntax-typescript',
                                    { isTSX: true },
                                ],
                                [
                                    '@babel/plugin-transform-react-jsx',
                                    { runtime: 'automatic' },
                                ],
                            ],
                        },
                    },
                ],
            },
        ],
    },
};

export default serverConfig;
