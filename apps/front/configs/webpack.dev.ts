/// <reference types="webpack-dev-server" />

import { Configuration, DefinePlugin } from 'webpack';
import merge from 'webpack-merge';
import { baseConfig } from './webpack.common';

const devConfig: Configuration = {
    mode: 'development',
    plugins: [
        new DefinePlugin({
            __SOCKET_SERVICE__: '"ws://localhost:1234"',
            __IMAGES_SERVICE__: '"http://localhost:3001"',
            __STATIC_IMAGES_SERVICE__: '"http://localhost:3001/image"',
            __YA_APP_ID__: '"14ea8bd7381c4e9f88e056600fb43477"',
            __SERVER_SERVICE__: '"http://127.0.0.1:5001"',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.scss$/,
                exclude: /quill\.(core|bubble|snow)\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: '[hash:8]',
                            },
                        },
                    },
                    'sass-loader',
                ],
            },
            {
                test: /\.css$/,
                include: /quill\.(core|bubble|snow)\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(jpeg|jpg|png)$/,
                use: ['file-loader'],
            },
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'esbuild-loader',
                        options: {
                            loader: 'tsx',
                        },
                    },
                    {
                        loader: 'babel-loader',
                        options: {
                            plugins: [
                                [
                                    '@babel/plugin-syntax-typescript',
                                    { isTSX: true },
                                ],
                                [
                                    '@babel/plugin-transform-react-jsx',
                                    { runtime: 'automatic' },
                                ],
                            ],
                        },
                    },
                ],
            },
        ],
    },
};

export default merge(baseConfig, devConfig);
