/// <reference types="webpack-dev-server" />

import { Configuration } from 'webpack';
import merge from 'webpack-merge';
import { ESBuildMinifyPlugin } from 'esbuild-loader';
import { baseConfig } from './webpack.common';
import TSCheckWebpackPlugin from 'fork-ts-checker-webpack-plugin';

const prodConfig: Configuration = {
    mode: 'production',
    plugins: [new TSCheckWebpackPlugin()],
    optimization: {
        minimize: true,
        minimizer: [new ESBuildMinifyPlugin()],
    },
};

export default merge(baseConfig, prodConfig);
