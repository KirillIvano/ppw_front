import { Editor } from '@front/parts';
import { useDocId } from '../../hooks/useDocId';

export const DocPageEditor = () => {
    const docId = useDocId();

    return <Editor docId={docId} />;
};
