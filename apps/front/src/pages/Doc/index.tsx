import { DocPageEditor } from './components/DocPageEditor';

export const DocPage = () => {
    return (
        <div>
            <DocPageEditor />
        </div>
    );
};
