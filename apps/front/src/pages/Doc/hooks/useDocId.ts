import { useParams } from 'react-router-dom';

export const useDocId = (): string => useParams().docId as string;
