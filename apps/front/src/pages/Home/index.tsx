import { useSelector } from 'react-redux';

import { selectIsAuthorized } from '@front/features/auth/store/selectors';

import { AuthorizedHome } from './components/AuthorizedHome';
import { Welcome } from './components/Welcome';

export const Home = () => {
    const isAuthorized = useSelector(selectIsAuthorized);

    return isAuthorized ? <AuthorizedHome /> : <Welcome />;
};
