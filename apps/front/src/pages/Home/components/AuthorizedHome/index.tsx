import { getDocuments } from '@front/features/docs/actions';
import {
    selectDocumentsList,
    selectDocumentsStatus,
} from '@front/features/docs/selectors';
import { ErrorPage } from '@front/parts/ErrorPage';
import { LoaderPage } from '@front/components/LoaderPage';
import { AppDispatch } from '@front/stores';
import { ResponsiveContainer } from '@front/uikit';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import css from './styles.module.scss';
import { DocumentCard } from '@front/components/DocumentCard';

const Content = () => {
    const documents = useSelector(selectDocumentsList);

    return (
        <ResponsiveContainer className={css.authorizedHome}>
            <h2 className={css.heading}>Ваши документы</h2>
            <div className={css.documents}>
                {documents.map(({ name, docId }) => (
                    <DocumentCard key={docId} docId={docId} name={name} />
                ))}
            </div>
        </ResponsiveContainer>
    );
};
export const AuthorizedHome = () => {
    const dispatch = useDispatch<AppDispatch>();
    const documentsStatus = useSelector(selectDocumentsStatus);

    const handleError = () => dispatch(getDocuments(null));

    useEffect(() => {
        dispatch(getDocuments(null));
    }, [dispatch]);

    if (documentsStatus === 'idle' || documentsStatus === 'pending')
        return <LoaderPage caption="Загружаем документы" />;
    if (documentsStatus === 'error')
        return (
            <ErrorPage
                caption="Не удалось загрузить документы"
                retry={handleError}
            />
        );

    return <Content />;
};
