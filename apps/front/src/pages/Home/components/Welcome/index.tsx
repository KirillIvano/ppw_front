import { ResponsiveContainer } from '@front/uikit';
import css from './styles.module.scss';

export const Welcome = () => {
    return (
        <ResponsiveContainer className={css.welcome}>
            <div className={css.content}>
                <h1 className={css.heading}>Невероятно быстрый редактор</h1>
                <p className={css.paragraph}>
                    Присоединяйтесь к совместному редактированию после
                    регистрации
                </p>
            </div>

            <div className={css.image}></div>
        </ResponsiveContainer>
    );
};
