import { AuthButton } from '@front/components/AuthButton';
import {
    selectAuthStatus,
    selectUserInfo,
} from '@front/features/auth/store/selectors';
import { useSelector } from 'react-redux';

export const AuthTestPage = () => {
    const user = useSelector(selectUserInfo);
    const authStatus = useSelector(selectAuthStatus);

    return (
        <div>
            <AuthButton />
            Статус: {authStatus}
            {JSON.stringify(user)}
        </div>
    );
};
