import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { LoaderPage } from '@front/components/LoaderPage';
import { showMessage } from '@front/parts/Messages/store';
import { AppDispatch } from '@front/stores';
import { authorizeThunk } from '@front/features/auth/store/actions';
import { selectAuthStatus } from '@front/features/auth/store/selectors';

import { useAuthToken } from './hooks/useAuthToken';

export const AuthPage = () => {
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();

    const token = useAuthToken();
    const authStatus = useSelector(selectAuthStatus);

    useEffect(() => {
        if (authStatus !== 'idle') return navigate('/');
        if (!token) {
            dispatch(
                showMessage({
                    title: 'Нет токена в url',
                    messageType: 'error',
                }),
            );
            navigate('/');
            return;
        }

        dispatch(authorizeThunk(token));
    }, [token, authStatus, navigate, dispatch]);

    useEffect(() => {
        if (authStatus === 'success') navigate('/');
    }, [authStatus, navigate]);

    return <LoaderPage caption="Происходит авторизация" />;
};
