import { useMemo } from 'react';
import { parse } from 'qs';
import { useLocation } from 'react-router-dom';

export const useAuthToken = (): string | null => {
    const { hash } = useLocation();
    const parsedHash = useMemo(() => parse(hash.slice(1)), [hash]);

    return String(parsedHash['access_token']) ?? null;
};
