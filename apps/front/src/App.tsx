import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Routes } from 'react-router-dom';

import { AuthenticatedRoute } from './components/AuthenticatedRoute';
import { LoaderPage } from './components/LoaderPage';
import { initializeAuth } from './features/auth/store/actions';
import { selectInitStatus } from './features/auth/store/selectors';
import { AuthPage } from './pages/Auth';
import { AuthTestPage } from './pages/AuthTest';
import { DocPage } from './pages/Doc';
import { Home } from './pages/Home';
import { Heading } from './parts';
import { Messages } from './parts/Messages';
import { AppDispatch } from './stores';

export const App = () => {
    const dispatch = useDispatch<AppDispatch>();
    const initStatus = useSelector(selectInitStatus);

    /** Действия, которые должны выполниться один раз при запуске аппки */
    useEffect(() => {
        dispatch(initializeAuth(null));
    }, [dispatch]);

    if (initStatus === 'idle' || initStatus === 'pending')
        return <LoaderPage caption="Инициализация приложения" />;

    return (
        <>
            <Heading />
            <Messages />

            <Routes>
                <Route path="/" element={<Home />} />
                <Route
                    path="/doc/:docId"
                    element={
                        <AuthenticatedRoute>
                            <DocPage />
                        </AuthenticatedRoute>
                    }
                />
                <Route path="/auth" element={<AuthPage />} />

                {/** Тестовая страничка для просмотра авторизации */}
                <Route path="/auth-test" element={<AuthTestPage />} />
            </Routes>
        </>
    );
};
