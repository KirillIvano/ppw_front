export const timeout = (ms = 1000) => new Promise((r) => setTimeout(r, ms));
