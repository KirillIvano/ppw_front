import { AnyAction } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { AppDispatch, RootState } from '@front/stores';

export type ThunkApi = {
    dispatch: AppDispatch;
    getState: () => RootState;
};

export type Thunk<TArg, TRes> = TArg extends undefined
    ? () => ThunkAction<TRes, RootState, any, AnyAction>
    : (TArg: TArg) => ThunkAction<TRes, RootState, any, AnyAction>;

export const createThunk = <TArg, TRes>(
    fn: (arg: TArg, api: ThunkApi) => TRes,
): Thunk<TArg, TRes> => {
    return ((arg: TArg) => (dispatch: AppDispatch, getState: () => RootState) =>
        fn(arg, { dispatch, getState })) as unknown as Thunk<TArg, TRes>;
};

export const createGenericThunk = (
    fn: <TArg, TRes>(arg: TArg, api: ThunkApi) => TRes,
) => {
    return <TA, TR>(arg: TA) =>
        (dispatch: AppDispatch, getState: () => RootState) =>
            fn<TA, TR>(arg, { dispatch, getState });
};
