import { cond, startsWith, always } from 'ramda';

export type UploadedImageType = 'data' | 'url';

export const getImageType: (imgSrc: string) => UploadedImageType = cond([
    [startsWith('data'), always('data')],
    [startsWith('url'), always('url')],
]);
