import { timeout } from '../timeout';

export class TimeoutError extends Error {}

export const withTimeout = <T>(
    promise: Promise<T>,
    timeoutMs: number,
): Promise<T> =>
    Promise.race([
        promise,
        timeout(timeoutMs).then(() => {
            throw new TimeoutError();
        }),
    ]);
