export const parseJwt = (jwt: string): null | { exp: number } => {
    try {
        const metaPart = jwt.split('.')[0];
        const base64 = metaPart.replace(/-/g, '+').replace(/_/g, '/');

        const payload = decodeURIComponent(
            window
                .atob(base64)
                .split('')
                .map(function (c) {
                    return (
                        '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
                    );
                })
                .join(''),
        );

        return JSON.parse(payload);
    } catch {
        return null;
    }
};
