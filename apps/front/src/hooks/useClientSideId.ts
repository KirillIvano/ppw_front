import { type DependencyList, useMemo } from 'react';

let i = 0;
/**
 * Хук получает уникальный идентификатор, предназначенный для клиентских сценариев
 * Если компонент должен использоваться при серверном рендеринге, надо юзать useId из react
 */
export const useClientSideId = (deps?: DependencyList): string => {
    const id = useMemo(() => `${++i}`, deps);

    return id;
};
