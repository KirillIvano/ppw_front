import { documentsSlice } from '@front/features/docs/slice';
import { imageUploadSlice } from '@front/parts/Editor/stores/imageUpload';
import { messagesReducer } from '@front/parts/Messages/store';
import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { usersSlice } from '../features/auth/store/slice';

// научиться инициализировать динамически
const reducer = combineReducers({
    messages: messagesReducer.reducer,
    user: usersSlice.reducer,
    documents: documentsSlice.reducer,
    editor: combineReducers({
        imageUpload: imageUploadSlice.reducer,
    }),
});

export const store = configureStore({
    reducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
