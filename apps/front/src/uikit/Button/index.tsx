import cn from 'classnames';
import {
    AnchorHTMLAttributes,
    ReactNode,
    type ButtonHTMLAttributes,
} from 'react';

import css from './styles.module.scss';

type CommonProps = {
    icon?: string;
    iconPosition?: 'before' | 'after';
};

const ButtonIcon = ({ icon }: { icon: string }) => (
    <img key="img" src={icon}></img>
);

export type ButtonBaseProps = CommonProps & {
    children: ReactNode;
};

export const ButtonBase = ({
    icon,
    iconPosition = 'before',
    children,
}: ButtonBaseProps) => {
    if (icon)
        return (
            <div
                className={cn(css.withIcon, {
                    [`${css.withIcon}_${iconPosition}`]: true,
                })}
            >
                <ButtonIcon key="1" icon={icon} />
                {children}
            </div>
        );

    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <>{children}</> ?? null;
};

export type ButtonProps = CommonProps & ButtonHTMLAttributes<HTMLButtonElement>;

export const Button = ({
    className,
    icon,
    iconPosition,
    children,
    ...props
}: ButtonProps) => (
    <button {...props} className={cn(css.button, className)}>
        <ButtonBase icon={icon} iconPosition={iconPosition}>
            {children}
        </ButtonBase>
    </button>
);

export type ButtonLinkProps = CommonProps &
    AnchorHTMLAttributes<HTMLAnchorElement>;

export const ButtonLink = ({
    className,
    icon,
    iconPosition,
    children,
    ...props
}: ButtonLinkProps) => (
    <a {...props} className={cn(css.button, className)}>
        <ButtonBase icon={icon} iconPosition={iconPosition}>
            {children}
        </ButtonBase>
    </a>
);
