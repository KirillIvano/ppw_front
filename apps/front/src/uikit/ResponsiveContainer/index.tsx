import { HTMLAttributes } from 'react';
import cn from 'classnames';

import css from './styles.module.scss';

export const ResponsiveContainer = ({
    className,
    ...props
}: HTMLAttributes<HTMLDivElement>) => {
    return <div {...props} className={cn(className, css.container)}></div>;
};
