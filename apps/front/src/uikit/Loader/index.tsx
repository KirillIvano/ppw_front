import { type CSSProperties } from 'react';

import styles from './styles.module.scss';

export type LoaderProps = {
    size?: number;
};

export const Loader = ({ size }: LoaderProps) => {
    const variables = {
        '--loader-size': `${size}px`,
    } as CSSProperties;

    return <div style={variables} className={styles.loaderBox} />;
};
