import { useId, InputHTMLAttributes } from 'react';
import cn from 'classnames';

import css from './styles.module.scss';

export type InputProps = {
    containerClass?: string;
    label?: string;
} & InputHTMLAttributes<HTMLInputElement>;

export const Input = ({ label, containerClass, ...rest }: InputProps) => {
    const id = useId();

    return (
        <div className={cn(containerClass, css.inputContainer)}>
            {label && (
                <label className={css.label} htmlFor={id}>
                    {label}
                </label>
            )}
            <input className={cn(css.input)} id={id} {...rest}></input>
        </div>
    );
};
