import fastify from 'fastify';
import { renderToString } from 'react-dom/server';
import fastifyStatic from '@fastify/static';

import { App } from '@front/apps/readonly/App';
import { getDocContent } from '@front/services/bff/documents';

import { renderReadonlyDocPage } from './templates/readonlyDoc';

const app = fastify();

app.get<{ Querystring: { key: string }; Params: { docId: string } }>(
    '/readonly-doc/:docId',
    async (req, res) => {
        const { docId } = req.params;
        const { key } = req.query;

        let content: string;
        try {
            const docRes = await getDocContent(docId, key);

            if (docRes.ok) {
                content = docRes.data.content;
            } else {
                return res.redirect('/');
            }
        } catch (e) {
            return res.redirect('/');
        }

        const app = <App content={JSON.parse(content)} />;
        const renderedApp = renderToString(app);

        const renderedPage = renderReadonlyDocPage(renderedApp, content);

        res.type('text/html');
        res.send(renderedPage);
    },
);

app.register(fastifyStatic, {
    root: __dirname,
    prefix: '/assets/',
    wildcard: true,
});

app.setNotFoundHandler((_, res) => {
    res.sendFile('index.html');
});

app.listen(3500);
