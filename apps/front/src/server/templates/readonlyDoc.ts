export const renderReadonlyDocPage = (
    content: string,
    documentState: string,
) => `
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>readonly document</title>
</head>
<body>
    <div class="app" id="root">${content}</div>

    <script>window.__INITIAL_DOC__ = ${documentState};</script>
    <script defer src="/assets/read.js"></script>
</body>
</html>
`;
