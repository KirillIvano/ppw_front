const REQUEST_STATUSES = {
    idle: 'idle',
    pending: 'pending',
    error: 'error',
    success: 'success',
} as const;

export type RequestStatus =
    typeof REQUEST_STATUSES[keyof typeof REQUEST_STATUSES];
