import { AppDispatch } from '@front/stores';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getDocuments } from './actions';
import { selectDocumentsStatus } from './selectors';

export const useDocsInit = () => {
    const dispatch = useDispatch<AppDispatch>();

    const documentsStatus = useSelector(selectDocumentsStatus);

    useEffect(() => {
        if (documentsStatus === 'idle') {
            dispatch(getDocuments(null));
        }
    }, [documentsStatus, dispatch]);
};
