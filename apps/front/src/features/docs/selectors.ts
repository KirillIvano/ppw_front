import { createSelector } from '@reduxjs/toolkit';
import { DocumentsStoreSlice } from './types';

export const selectDocumentsStore = (state: DocumentsStoreSlice) =>
    state.documents;

const selectDocumentsIds = (state: DocumentsStoreSlice) =>
    selectDocumentsStore(state).docsList;
const selectDocumentsRecord = (state: DocumentsStoreSlice) =>
    selectDocumentsStore(state).docs;

export const selectDocumentsList = createSelector(
    selectDocumentsIds,
    selectDocumentsRecord,
    (ids, record) => ids.map((id) => record[id]).filter((x) => x !== undefined),
);

export const selectDocumentsStatus = (state: DocumentsStoreSlice) =>
    selectDocumentsStore(state).documentsUpdateStatus;
