import { SERVER_SERVICE } from '@front/constants';
import { showMessage } from '@front/parts/Messages/store';
import { createThunk } from '@front/utils/createThunk';
import type { GetDocumentsResponse } from '@ppw/shared/integrations/bff';

import { authorizedFetch } from '../auth/store/actions';
import { documentsSlice } from './slice';

export const getDocumentsRequest = createThunk((_: undefined, { dispatch }) =>
    dispatch(
        authorizedFetch<GetDocumentsResponse>({
            url: `${SERVER_SERVICE}/api/docs`,
        }),
    ),
);

export const getDocuments = createThunk(async (_, { dispatch }) => {
    dispatch(documentsSlice.actions.setDocumentLoadStatus('pending'));

    const res = await dispatch(getDocumentsRequest());

    if (res.ok) {
        dispatch(documentsSlice.actions.setDocumentLoadStatus('success'));
        dispatch(documentsSlice.actions.addDocuments(res.data));
    } else {
        dispatch(documentsSlice.actions.setDocumentLoadStatus('error'));
        dispatch(
            showMessage({
                title: 'Не удалось загрузить документы',
                messageType: 'error',
            }),
        );
    }
});
