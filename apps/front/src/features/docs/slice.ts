import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import type { DocumentData } from '@ppw/shared/types/document';
import type { RequestStatus } from '@front/types/status';
import type { DocumentsStore } from './types';

const initialState: DocumentsStore = {
    docs: {},
    docsList: [],
    documentsUpdateStatus: 'idle',
};

export const documentsSlice = createSlice({
    name: 'documentsSlice',
    initialState: initialState,
    reducers: {
        addDocuments(
            state,
            { payload: { docs } }: PayloadAction<{ docs: DocumentData[] }>,
        ) {
            const docsIds = [];

            for (const doc of docs) {
                state.docs[doc.docId] = doc;
                docsIds.push(doc.docId);
            }

            state.docsList = Array.from(
                new Set([...docsIds, ...state.docsList]),
            );
        },
        setDocumentLoadStatus(
            state,
            { payload: status }: PayloadAction<RequestStatus>,
        ) {
            state.documentsUpdateStatus = status;
        },
    },
});
