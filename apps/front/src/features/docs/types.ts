import type { ShortDocumentInfo } from '@ppw/shared/types/document';
import type { RequestStatus } from '@front/types/status';

export type DocumentsStore = {
    docs: Record<string, ShortDocumentInfo>;
    docsList: string[];
    documentsUpdateStatus: RequestStatus;
};

export type DocumentsStoreSlice = {
    documents: DocumentsStore;
};
