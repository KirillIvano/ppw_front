import { YA_APP_ID } from '@front/constants';

export const generateYandexAuthUrl = (redirectUri: string) =>
    `https://oauth.yandex.ru/authorize?response_type=token&client_id=${YA_APP_ID}&redirect_uri=${redirectUri}`;
