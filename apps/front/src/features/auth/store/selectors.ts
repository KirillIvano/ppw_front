import { UserData } from '@ppw/shared/types/user';
import { createSelector } from '@reduxjs/toolkit';
import { UserStoreSlice } from './types';

export const selectUserStore = (store: UserStoreSlice) => store.user;

export const selectAuthStatus = createSelector(
    selectUserStore,
    (store) => store.authStatus,
);

export const selectInitStatus = createSelector(
    selectUserStore,
    (store) => store.initStatus,
);

export const selectUserInfo = createSelector(
    selectUserStore,
    (store) => store.user,
);

export const selectUserInfoSafe = selectUserInfo as (
    store: UserStoreSlice,
) => UserData;

export const selectIsAuthorized = createSelector(
    selectUserStore,
    (store) => !!store.user,
);
