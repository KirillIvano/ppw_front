import { showMessage } from '@front/parts/Messages/store';
import {
    authorize,
    logoutRequest,
    refreshTokenRequest,
} from '@front/services/bff';
import { AppDispatch } from '@front/stores';
import { createThunk } from '@front/utils/createThunk';
import { request } from '@front/utils/request';
import { UserData } from '@ppw/shared/types/user';

import { usersSlice } from './slice';

const USER_KEY = 'user_info';
const ACCESS_TOKEN_KEY = 'access_token';

const persistUser = (user: UserData, accessToken: string) => {
    localStorage.setItem(USER_KEY, JSON.stringify(user));
    localStorage.setItem(ACCESS_TOKEN_KEY, accessToken);
};

const clearUserData = () => {
    localStorage.removeItem(USER_KEY);
    localStorage.removeItem(ACCESS_TOKEN_KEY);
};

const getAccessToken = (): string | null =>
    localStorage.getItem(ACCESS_TOKEN_KEY);

const getUserInfo = (): UserData | null => {
    try {
        const rawKey = localStorage.getItem(USER_KEY);
        if (!rawKey) return null;

        const parsedUser = JSON.parse(rawKey);

        /** @todo: add validation */
        return parsedUser as UserData;
    } catch {
        return null;
    }
};

export const authorizeThunk = createThunk(
    async (token: string, { dispatch }) => {
        dispatch(usersSlice.actions.authorizeLoading());

        const res = await authorize(token);

        if (!res.ok) {
            dispatch(
                showMessage({
                    title: 'Не удалось авторизоваться',
                    messageType: 'error',
                }),
            );
            dispatch(usersSlice.actions.authorizeFailed());

            return;
        }

        const { user, accessToken } = res.data;

        dispatch(usersSlice.actions.authorize(user));
        persistUser(user, accessToken);

        dispatch(showMessage({ title: 'Вы успешно авторизованы' }));
    },
);

export const logout = createThunk(async (_: undefined, { dispatch }) => {
    await logoutRequest();
    clearUserData();

    dispatch(usersSlice.actions.logout());
    dispatch(showMessage({ title: 'Вы успешно вышли' }));
});

export type AuthorizedFetchParams = RequestInit & {
    url: string;
};

export const authorizedFetch =
    <TRes extends Record<string, unknown>>({
        url,
        ...options
    }: AuthorizedFetchParams) =>
    async (dispatch: AppDispatch) => {
        const authorizedRequestBase = () => {
            const accessToken = getAccessToken();
            const authHeaders = {
                authorization: `Bearer ${accessToken}`,
            };

            return request<TRes>(url, {
                credentials: 'include',
                ...options,
                headers: {
                    ...authHeaders,
                    ...options?.headers,
                },
            });
        };

        const res = await authorizedRequestBase();
        if (res.status !== 401) {
            return res;
        }

        // пробуем перезапросить токен сразу
        const refreshRes = await refreshTokenRequest();
        if (!refreshRes.ok) {
            if (refreshRes.status === 401) {
                dispatch(logout());
                dispatch(
                    showMessage({
                        title: 'Войдите в систему повторно',
                        messageType: 'error',
                    }),
                );
            } else {
                dispatch(
                    showMessage({
                        title: 'Запрос завершился неудачей, попробуйте перезагрузить страницу',
                        messageType: 'error',
                    }),
                );
            }

            return res;
        }

        // делаем повторный запрос
        const retryRes = await authorizedRequestBase();
        if (retryRes.status === 401) {
            dispatch(logout());
        }

        return retryRes;
    };

export const initializeAuth = createThunk(async (_: unknown, { dispatch }) => {
    dispatch(usersSlice.actions.setInitStatus('pending'));

    const user = getUserInfo();

    if (!user) {
        clearUserData();
        dispatch(usersSlice.actions.setInitStatus('error'));
        return;
    }

    const refreshRes = await refreshTokenRequest();

    if (!refreshRes.ok) {
        if (refreshRes.status === 401) {
            await dispatch(logout());
        } else {
            dispatch(
                showMessage({
                    title: 'Не удалось получить авторизационные данные, попробуйте позже',
                    messageType: 'error',
                }),
            );
        }

        return dispatch(usersSlice.actions.setInitStatus('error'));
    }

    dispatch(usersSlice.actions.setInitStatus('success'));
    dispatch(usersSlice.actions.authorize(user));
});
