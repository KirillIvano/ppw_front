import { RequestStatus } from '@front/types/status';
import { UserData } from '@ppw/shared/types/user';

export type AuthStatus = 'idle' | 'authorized' | 'failed';

export type UserStoreType = {
    initStatus: RequestStatus;
    authStatus: RequestStatus;
    user: UserData | null;
};

export type UserStoreSlice = {
    user: UserStoreType;
};
