import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { UserData } from '@ppw/shared/types/user';
import { RequestStatus } from '@front/types/status';

import type { UserStoreType } from './types';

const initialState: UserStoreType = {
    user: null,
    authStatus: 'idle',
    initStatus: 'idle',
};

export const usersSlice = createSlice({
    name: 'userSlice',
    initialState: initialState as UserStoreType,
    reducers: {
        authorizeLoading(state) {
            state.authStatus = 'pending';
        },
        authorizeFailed(state) {
            state.authStatus = 'error';
        },
        authorize(state, { payload: user }: PayloadAction<UserData>) {
            state.authStatus = 'success';
            state.user = user;
        },
        logout(state) {
            state.user = null;
        },
        setInitStatus(
            state,
            { payload: status }: PayloadAction<RequestStatus>,
        ) {
            state.initStatus = status;
        },
    },
});
