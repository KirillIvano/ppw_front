import * as Y from 'yjs';
import { useEffect, useState, memo } from 'react';
import { WebsocketProvider } from 'y-websocket';
import { QuillDeltaToHtmlConverter } from 'quill-delta-to-html';

import { SOCKET_SERVICE } from '@front/constants';
import { ResponsiveContainer } from '@front/uikit';

import css from './styles.module.scss';
import { DocumentState } from '@ppw/shared/types/document';

const convert = (state: DeltaOperation[]) => {
    const converter = new QuillDeltaToHtmlConverter(state);
    const htmlContent = converter.convert();

    return htmlContent;
};

const useDocument = (content: DeltaOperation[]) => {
    const [docContent, setDocContent] = useState(() => convert(content));

    useEffect(() => {
        const doc = new Y.Doc();
        const text = doc.getText('quill_persisted');

        new WebsocketProvider(
            SOCKET_SERVICE,
            '21c819ed7164c2b88c8f05cd27810a9c',
            doc,
        );

        text.observe(() => {
            const converter = new QuillDeltaToHtmlConverter(text.toDelta());
            const htmlContent = converter.convert();

            setDocContent(htmlContent);
        });
    }, []);

    return docContent;
};

export type ReadonlyDocProps = {
    content: DocumentState;
    docId: string;
};

export const ReadonlyDoc = memo(({ content }: ReadonlyDocProps) => {
    const doc = useDocument(content);

    return (
        <ResponsiveContainer className={css.content}>
            <div dangerouslySetInnerHTML={{ __html: doc }} />
        </ResponsiveContainer>
    );
});
ReadonlyDoc.displayName = 'ReadonlyDocWrapper';
