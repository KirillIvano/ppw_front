import { useLayoutEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { AppDispatch } from '@front/stores';

import { messagesReducer } from './store';
import { selectMessages } from './store/selectors';
import Message from './Message';
import css from './styles.module.scss';

export const Messages = () => {
    const messages = useSelector(selectMessages);
    const dispatch = useDispatch<AppDispatch>();

    // clear state on first render
    useLayoutEffect(() => {
        dispatch(messagesReducer.actions.clearMessages());
    }, [dispatch]);

    return (
        <div className={css.messagesContainer}>
            {messages.map((message) => (
                <Message key={message.id} {...message} />
            ))}
        </div>
    );
};
