import classnames from 'classnames';

import css from './styles.module.scss';
import { useDispatch } from 'react-redux';
import { messagesReducer, MessageType } from './store';

export type MessageProps = {
    id: string;
    title: string;
    messageType?: MessageType;
};

const Message = ({ id, title, messageType = 'success' }: MessageProps) => {
    const dispatch = useDispatch();

    const handleClick = () => {
        dispatch(messagesReducer.actions.removeMessage(id));
    };

    return (
        <div
            className={classnames(css.message, css[`message_${messageType}`])}
            onClick={handleClick}
        >
            <p className={css.messageText}>{title}</p>
        </div>
    );
};

export default Message;
