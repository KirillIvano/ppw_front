import { createThunk } from '@front/utils/createThunk';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getMessagesStore } from './selectors';

export type MessageType = 'success' | 'error';

export type Message = {
    title: string;
    id: string;
    timeout?: number;
    messageType?: 'success' | 'error';
};

export type MessagesState = {
    messages: Message[];
    lastGeneratedId: number;
};

const initialState: MessagesState = {
    messages: [],
    lastGeneratedId: 1,
};

export const messagesReducer = createSlice({
    name: 'editor/messages',
    initialState,
    reducers: {
        addMessage(state, { payload: message }: PayloadAction<Message>) {
            state.messages = [...state.messages, message];
        },
        removeMessage(state, { payload: messageId }: PayloadAction<string>) {
            state.messages = state.messages.filter((m) => m.id !== messageId);
        },
        updateGeneratedId(state) {
            state.lastGeneratedId++;
        },
        clearMessages(state) {
            state.messages = [];
        },
    },
});

export type ShowMessageParams = Omit<Message, 'id'> & { id?: string };

const getUpdatedCounter = createThunk<unknown, number>(
    (_, { getState, dispatch }) => {
        dispatch(messagesReducer.actions.updateGeneratedId());

        const messagesState = getMessagesStore(getState());

        return messagesState.lastGeneratedId;
    },
);

const DEFAULT_TIMEOUT = 2000;

export const showMessage = createThunk(
    async (message: ShowMessageParams, { dispatch }) => {
        const finalizedMessage: Message = {
            ...message,
            id: message.id ?? dispatch(getUpdatedCounter(null)),
        };

        dispatch(messagesReducer.actions.addMessage(finalizedMessage));

        setTimeout(
            () =>
                dispatch(
                    messagesReducer.actions.removeMessage(finalizedMessage.id),
                ),
            message.timeout ?? DEFAULT_TIMEOUT,
        );
    },
);
