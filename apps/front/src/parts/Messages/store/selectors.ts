import { RootState } from '@front/stores';

export const getMessagesStore = (state: RootState) => state.messages;
export const selectMessages = (state: RootState) =>
    getMessagesStore(state).messages;
