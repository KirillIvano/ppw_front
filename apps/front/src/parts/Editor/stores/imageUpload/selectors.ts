import { RootState } from '@front/stores';

export const getImageUploadStore = (state: RootState) =>
    state.editor.imageUpload;

export const selectImageUploadStatus = (state: RootState) =>
    getImageUploadStore(state).status;
