import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { uploadImageFile } from '@front/services/images';
import { withTimeout } from '@front/utils/promises/withTimeout';
import { STATIC_IMAGES_SERVICE } from '@front/constants';
import { showMessage } from '@front/parts/Messages/store';
import { AppDispatch } from '@front/stores';

export type ImageUploadStateType =
    | {
          status: 'idle' | 'pending' | 'error';
          image: null;
      }
    | {
          status: 'fulfilled';
          image: string;
      };

const initialState: ImageUploadStateType = {
    status: 'idle',
    image: null,
};

export const imageUploadSlice = createSlice({
    name: 'imageUploadSlice',
    initialState: initialState as ImageUploadStateType,
    reducers: {
        clear: (state: ImageUploadStateType) => {
            state.status = 'idle';
            state.image = null;
        },
    },
    extraReducers(builder) {
        builder.addCase(imageUpload.fulfilled, (_, { payload }) => ({
            status: 'fulfilled',
            image: payload,
        }));

        builder.addCase(imageUpload.rejected, (state) => {
            state.status = 'error';
        });

        builder.addCase(imageUpload.pending, (state) => {
            state.status = 'pending';
        });
    },
});

export const imageUpload = createAsyncThunk(
    'imageUpload',
    async (image: File, { dispatch }): Promise<string> => {
        const uploadResult = await withTimeout(uploadImageFile(image), 1000);

        if (uploadResult.ok) {
            return `${STATIC_IMAGES_SERVICE}/${uploadResult.data.image}`;
        } else {
            (dispatch as AppDispatch)(
                showMessage({
                    title: 'Не удалось загрузить изображение',
                    messageType: 'error',
                }),
            );
            throw new Error();
        }
    },
);
