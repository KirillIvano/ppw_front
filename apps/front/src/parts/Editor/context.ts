import { createContext, useContext } from 'react';
import { type EditorCtx } from './types';

export const EditorContext = createContext<EditorCtx>(
    null as unknown as EditorCtx,
);
export const useEditorContext = () => useContext(EditorContext);
