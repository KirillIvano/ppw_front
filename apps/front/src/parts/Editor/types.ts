export type EditorCtx = {
    docId: string;
};

export type ImageUploadSource = 'user' | 'prog';

export type ImageObject = {
    src: string;
    source: ImageUploadSource;
};
