import { Loader } from '@front/uikit/Loader';
import { useEffect, type RefObject } from 'react';
import Modal from 'react-modal';
import { useDispatch, useSelector } from 'react-redux';

import { type AppDispatch } from '@front/stores';

import { imageUploadSlice } from '../../stores/imageUpload';
import { getImageUploadStore } from '../../stores/imageUpload/selectors';

import css from './styles.module.scss';

export type EditorImageModalProps = {
    onLoad: RefObject<(image: string) => void>;
};

export const EditorImageModal = ({ onLoad }: EditorImageModalProps) => {
    const imageUploadStore = useSelector(getImageUploadStore);
    const dispatch = useDispatch<AppDispatch>();

    useEffect(() => {
        if (imageUploadStore.status === 'fulfilled') {
            onLoad.current?.(imageUploadStore.image);
            dispatch(imageUploadSlice.actions.clear());
        } else if (imageUploadStore.status === 'error') {
            dispatch(imageUploadSlice.actions.clear());
        }
    }, [imageUploadStore, onLoad, dispatch]);

    return (
        <Modal
            isOpen={imageUploadStore.status === 'pending'}
            className={css.imageModal}
            overlayClassName={css.imageModalOverlay}
            shouldCloseOnOverlayClick={false}
            shouldCloseOnEsc={false}
        >
            <div>
                <h2>Изображение загружается</h2>
                <Loader />
            </div>
        </Modal>
    );
};
