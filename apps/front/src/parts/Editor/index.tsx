import React, { useLayoutEffect, useRef } from 'react';
import Quill from 'quill';
import * as Y from 'yjs';
import { WebsocketProvider } from 'y-websocket';
import { QuillBinding } from 'y-quill';
import QuillCursors from 'quill-cursors';

import 'quill/dist/quill.core.css';
import 'quill/dist/quill.bubble.css';
import 'quill/dist/quill.snow.css';

import { ResponsiveContainer } from '@front/uikit';
import { useClientSideId } from '@front/hooks/useClientSideId';
import { AppDispatch } from '@front/stores';

import { EditorContext, useEditorContext } from './context';
import { SOCKET_SERVICE } from '@front/constants';
import { useDispatch, useSelector } from 'react-redux';
import { EditorImageModal } from './components/ImageUploadModal';
import { imageUpload } from './stores/imageUpload';
import css from './styles.module.scss';
import { selectUserInfoSafe } from '@front/features/auth/store/selectors';

const getToolbarId = (id: string): string => `editor-toolbar__${id}`;

const initQuill = () => {
    Quill.register('modules/cursors', QuillCursors);
};

initQuill();

export const EditorBase = () => {
    const dispatch = useDispatch<AppDispatch>();
    const { docId } = useEditorContext();
    const toolbarId = useClientSideId();
    const editorElRef = useRef<HTMLDivElement | null>(null);
    const editorRef = useRef<Quill | null>(null);
    const user = useSelector(selectUserInfoSafe);

    const handleImageUpload = useRef<((image: string) => void) | null>(null);

    useLayoutEffect(() => {
        if (!editorElRef.current) return;

        const doc = new Y.Doc();
        const text = doc.getText('quill');

        const wsProvider = new WebsocketProvider(SOCKET_SERVICE, docId, doc);

        const editor = new Quill(editorElRef.current, {
            formats: [
                'bold',
                'italic',
                'link',
                'size',
                'list',
                'code-block',
                'header',
                'image',
            ],
            modules: {
                cursors: true,
                toolbar: {
                    container: [
                        [{ header: [1, 2, 3, 4, 5, false] }],
                        ['bold', 'italic'],
                        ['link', 'code-block'],
                        [{ list: 'ordered' }, { list: 'bullet' }],
                        ['image'],
                    ],
                    handlers: {
                        image: () => {
                            const tempInput = document.createElement('input');

                            tempInput.setAttribute('type', 'file');
                            tempInput.setAttribute(
                                'accept',
                                'image/jpeg,image/png',
                            );

                            tempInput.addEventListener('change', () => {
                                const range = editor.getSelection(true);

                                const file = tempInput.files?.[0] ?? null;
                                if (!file) return;

                                dispatch(imageUpload(file));

                                handleImageUpload.current = (image: string) =>
                                    editor.insertEmbed(
                                        range.index,
                                        'image',
                                        image,
                                    );
                            });

                            tempInput.click();
                        },
                    },
                },
            },
            placeholder: 'Happy typing...',
            theme: 'snow',
        });

        editorRef.current = editor;

        new QuillBinding(text, editor, wsProvider.awareness);

        wsProvider.awareness.setLocalStateField('user', {
            name: user.login,
            color: 'blue',
        });

        // do nothing when image is pasted
        editor.clipboard.addMatcher('IMG', (node, delta) => delta.slice(0, 0));
    }, [toolbarId, docId, user, dispatch]);

    return (
        <>
            <div>document id: {docId}</div>
            <EditorImageModal onLoad={handleImageUpload} />
            <ResponsiveContainer className={css.editor}>
                <div id={getToolbarId(toolbarId)}></div>
                <div ref={editorElRef} />
            </ResponsiveContainer>
        </>
    );
};

export type EditorProps = {
    docId: string;
};

export const Editor = React.memo(({ docId }: EditorProps) => {
    return (
        <EditorContext.Provider value={{ docId }}>
            <EditorBase />
        </EditorContext.Provider>
    );
});
Editor.displayName = 'Editor';
