import { Button } from '@front/uikit/Button';
import css from './styles.module.scss';

export type LoaderPageProps = {
    caption: string;
    retry?: () => void;
};

export const ErrorPage = ({ caption, retry }: LoaderPageProps) => (
    <div className={css.errorPage}>
        <div className={css.errorIcon} />
        <h2 className={css.errorCaption}>{caption}</h2>

        <Button onClick={retry}>Перезагрузить</Button>
    </div>
);
