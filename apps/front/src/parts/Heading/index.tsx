import { ReactNode } from 'react';

import { ResponsiveContainer } from '@front/uikit';

import css from './styles.module.scss';

type LogoProps = {
    onLogoClick?: () => void;
};

const Logo = ({ onLogoClick }: LogoProps) => (
    <a onClick={onLogoClick} className={css.logo}>
        <div className={css.logoParticle} />
        <div className={css.logoName}>PAPER WEIGHT</div>
    </a>
);

export type HeadingProps = {
    onLogoClick?: () => void;
    rightContent?: ReactNode;
};

export const Heading = ({ onLogoClick, rightContent }: HeadingProps) => (
    <ResponsiveContainer>
        <header className={css.header}>
            <Logo onLogoClick={onLogoClick} />

            <div className={css.rightContent}>{rightContent}</div>
        </header>
    </ResponsiveContainer>
);
