import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { App } from './App';
import { store } from './stores';

import './main.scss';

const WrappedApp = () => (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

createRoot(document.getElementById('root') as HTMLDivElement).render(
    <WrappedApp />,
);
