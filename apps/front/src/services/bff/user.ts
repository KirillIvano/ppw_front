import { SERVER_SERVICE } from '@front/constants';
import { request } from '@front/utils/request';

import { AuthorizeResponse } from '@ppw/shared/integrations/bff';

export const authorize = (token: string) =>
    request<AuthorizeResponse>(`${SERVER_SERVICE}/api/auth`, {
        method: 'POST',
        body: JSON.stringify({ token }),
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
        },
    });

export const logoutRequest = () =>
    request(`${SERVER_SERVICE}/api/logout`, { method: 'POST' });

export const refreshTokenRequest = async () => {
    return request(`${SERVER_SERVICE}/api/refresh`, {
        credentials: 'include',
    });
};
