import { SERVER_SERVICE } from '@front/constants';
import { request } from '@front/utils/request';

export const getDocContent = (docId: string, key: string) =>
    request<{ content: string }>(
        `${SERVER_SERVICE}/api/doc/content?key=${key}&docId=${docId}`,
    );
