import { IMAGES_SERVICE } from '@front/constants';
import { request, ResponseType } from '@front/utils/request';

const convertImageDataToFile = (imageData: string) => {
    /**
     * signature - data:<datatype>
     * data - base64 data
     */
    const [signature, data] = imageData.split(';base64,');
    const [, dataType] = signature.split(':');

    const characters = [window.atob(data)].map((char) => char.charCodeAt(0));
    const byteArray = Uint8Array.from(characters);

    const blob = new Blob([byteArray], { type: dataType });

    return blob;
};

/**
 * @deprected нужен только для аплоада через quill
 */
export const uploadImage = async (
    imageData: string,
): Promise<ResponseType<{ image: string }>> => {
    const blob = convertImageDataToFile(imageData);

    return request<{ image: string }>(`${IMAGES_SERVICE}/upload`, {
        method: 'POST',
        body: blob,
    });
};

export const uploadImageFile = async (
    imageFile: File,
): Promise<ResponseType<{ image: string }>> => {
    const formData = new FormData();

    formData.append('file', imageFile);

    return request<{ image: string }>(`${IMAGES_SERVICE}/upload`, {
        method: 'POST',
        body: formData,
    });
};
