import { hydrateRoot } from 'react-dom/client';

import '@front/main.scss';

import { App } from './App';

hydrateRoot(
    document.getElementById('root') as HTMLDivElement,
    <App content={window.__INITIAL_DOC__} />,
);
