import { Heading } from '@front/parts/Heading';
import { ReadonlyDoc } from '@front/parts/ReadonlyDoc';
import { memo } from 'react';

import css from './styles.module.scss';

const StaticHeading = memo(() => {
    const handleLogoClick = () => (location.pathname = '/');

    return <Heading onLogoClick={handleLogoClick} />;
});
StaticHeading.displayName = 'StaticHeading';

export const App = ({ content }: { content: DeltaOperation[] }) => (
    <div className={css.app}>
        <StaticHeading />

        <div className={css.editor}>
            <ReadonlyDoc content={content} />
        </div>
    </div>
);
