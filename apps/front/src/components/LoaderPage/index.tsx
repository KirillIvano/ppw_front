import { Loader } from '@front/uikit/Loader';
import css from './styles.module.scss';

export type LoaderPageProps = {
    caption: string;
};

export const LoaderPage = ({ caption }: LoaderPageProps) => (
    <div className={css.loaderPage}>
        <Loader size={64}></Loader>
        <h2 className={css.loaderCaption}>{caption}</h2>
    </div>
);
