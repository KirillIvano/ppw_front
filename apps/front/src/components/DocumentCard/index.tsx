import { Link } from 'react-router-dom';

import css from './styles.module.scss';

export type DocumentCardProps = {
    docId: string;
    name: string;

    classList?: string;
};

export const DocumentCard = ({ docId, name }: DocumentCardProps) => (
    <Link className={css.documentCard} to={`/doc/${docId}`}>
        <h3 className={css.name}>Имя документа: {name}</h3>
        <p className={css.identifier}>Идентификатор документа: {docId}</p>
    </Link>
);
