import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { generateYandexAuthUrl } from '@front/features/auth/utils';
import { Button, ButtonLink } from '@front/uikit/Button';
import { selectIsAuthorized } from '@front/features/auth/store/selectors';
import { logout } from '@front/features/auth/store/actions';
import { AppDispatch } from '@front/stores';

const LoginButton = ({ className }: { className?: string }) => {
    const [url, setUrl] = useState<string | undefined>();

    /** Рендерим только на клиенты */
    useEffect(() => {
        const { origin } = window.location;
        const authUrl = generateYandexAuthUrl(`${origin}/auth`);

        setUrl(authUrl);
    }, []);

    return (
        <ButtonLink className={className} href={url}>
            Войти
        </ButtonLink>
    );
};

const LogoutButton = ({ className }: { className?: string }) => {
    const dispatch = useDispatch<AppDispatch>();

    /** Рендерим только на клиенты */
    const handleClick = () => {
        dispatch(logout());
    };

    return (
        <Button onClick={handleClick} className={className}>
            Выйти
        </Button>
    );
};

export const AuthButton = ({ className }: { className?: string }) => {
    const isAuthorized = useSelector(selectIsAuthorized);

    if (!isAuthorized) {
        return <LoginButton className={className} />;
    }

    return <LogoutButton className={className} />;
};
