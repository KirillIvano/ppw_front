import {
    selectAuthStatus,
    selectIsAuthorized,
} from '@front/features/auth/store/selectors';
import { LoaderPage } from '@front/components/LoaderPage';
import { ReactNode } from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

export const AuthenticatedRoute = ({ children }: { children: ReactNode }) => {
    const authStatus = useSelector(selectAuthStatus);
    const isAuthorized = useSelector(selectIsAuthorized);

    if (authStatus === 'idle') return null;
    if (authStatus === 'pending')
        return <LoaderPage caption="Попытка авторизации..." />;
    if (!isAuthorized) return <Navigate to="/" />;

    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <>{children}</>;
};
