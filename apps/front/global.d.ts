declare module '*.scss' {
    const content: any;
    export default content;
}

declare module '*.jpeg' {
    const path: string;
    export default path;
}

declare module '*.jpg' {
    const path: string;
    export default path;
}

declare module '*.svg' {
    const path: string;
    export default path;
}

declare const __SOCKET_SERVICE__: string;
declare const __IMAGES_SERVICE__: string;
declare const __STATIC_IMAGES_SERVICE__: string;
declare const __YA_APP_ID__: string;
declare const __SERVER_SERVICE__: string;
