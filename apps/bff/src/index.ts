import register from 'module-alias';
import path from 'path';
import fastify from 'fastify';
import pino from 'pino';

register({ base: path.resolve(__dirname, '..') });

import 'dotenv/config';

import cors from '@fastify/cors';
import cookie from '@fastify/cookie';

import { registerErrorHandler } from './middlewares/error-middleware';
import { router } from './router';
import { PORT } from './settings';
import { connect } from './database';
import { startYjsSocket } from './yjs';

const log = pino();
const app = fastify({ logger: log });

app.register(cors, {
    credentials: true,
    origin: process.env.CLIENT_URL,
});
app.register(cookie);
registerErrorHandler(app);

app.register(router, { prefix: '/api' });

const start = async () => {
    try {
        await connect();
        await app.listen(PORT);
    } catch (e) {
        log.error(e);
    }
};

startYjsSocket();
start();
