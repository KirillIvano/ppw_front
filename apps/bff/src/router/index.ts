import { DocumentsController } from '../controllers/documents-controller';
import { FastifyInstance } from 'fastify';

import { UserController } from '../controllers/user-controller';
import { registerAuthMiddleware } from '../middlewares/auth-middleware';

export const router = (
    instance: FastifyInstance,
    opts: unknown,
    done: (err?: Error) => void,
) => {
    const userController = new UserController(instance);
    const documentsController = new DocumentsController(instance);

    instance.post('/auth', userController.authorize);
    instance.get('/refresh', userController.refresh);
    instance.post('/logout', userController.logout);

    instance.get('/doc/content', documentsController.getDocumentContent);

    instance.register((authIstance, _, done) => {
        registerAuthMiddleware(authIstance);

        authIstance.post('/doc', documentsController.createDocument);
        authIstance.put(
            '/doc/snapshot',
            documentsController.updateDocumentSnapshot,
        );
        authIstance.get('/docs', documentsController.getUserDocuments);

        authIstance.get('/users', userController.getUsers);

        done();
    });

    done();
};
