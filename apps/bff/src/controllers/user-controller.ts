import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { pick } from 'ramda';

import { ApiError } from '@bff/exceptions/api-error';
import { tokenService } from '@bff/service/token-service';
import { AuthorizePayload } from '@ppw/shared/integrations/bff';

import { userService } from '../service/user-service';
import { clientifyUserData } from '@bff/database/models/user-model';

export class UserController {
    constructor(private instance: FastifyInstance) {}

    authorize = async (
        req: FastifyRequest<{ Body: AuthorizePayload }>,
        res: FastifyReply,
    ) => {
        const { token } = req.body;
        if (!token) throw ApiError.BadRequest('token is required');

        const userData = await userService.authorize(token);

        res.cookie('refreshToken', userData.refreshToken, {
            maxAge: tokenService.REFRESH_EXPIERY,
            httpOnly: false,
            sameSite: 'none',
            secure: true,
        });

        const response = pick(['user', 'accessToken'], userData);

        return res.send(response);
    };

    async logout(req: FastifyRequest, res: FastifyReply) {
        const { refreshToken } = req.cookies;
        const token = await userService.logout(refreshToken);

        res.clearCookie('refreshToken');

        return res.send(token);
    }

    async refresh(req: FastifyRequest, res: FastifyReply) {
        const { refreshToken } = req.cookies;
        const userData = await userService.refresh(refreshToken);

        res.cookie('refreshToken', userData.refreshToken, {
            maxAge: tokenService.REFRESH_EXPIERY,
            httpOnly: true,
            sameSite: 'none',
            secure: true,
        });

        const response = pick(['user', 'accessToken'], userData);

        return res.send(response);
    }

    async getUsers(req: FastifyRequest, res: FastifyReply) {
        const users = await userService.getAllUsers();
        return res.send(users.map(clientifyUserData));
    }
}
