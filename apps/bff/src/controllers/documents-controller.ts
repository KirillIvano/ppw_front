import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { DocumentData } from '@ppw/shared/types/document';

import {
    DocumentModel,
    clientifyDoc,
    getShortDocumentInfo,
} from '@bff/database/models/document-model';
import { ApiError } from '@bff/exceptions/api-error';
import { documentsService } from '@bff/service/document-service';

export class DocumentsController {
    constructor(private instance: FastifyInstance) {}

    createDocument = async (
        req: FastifyRequest<{ Body: { name?: string } }>,
    ) => {
        if (!req.user) {
            throw ApiError.AuthorizeRequired();
        }

        const { userId } = req.user;
        const { name } = req.body;

        if (!name) throw ApiError.BadRequest('name required');

        const docId = await documentsService.generateDocId();

        const doc: DocumentData = {
            userId,
            docId,
            name,
            documentSnapshot: '',
        };

        const createdDoc = await DocumentModel.create<DocumentData>(doc);

        return { doc: clientifyDoc(createdDoc) };
    };

    getDocument = async (
        req: FastifyRequest<{ Body: { docId?: string; key: string } }>,
        res: FastifyReply,
    ) => {
        if (!req.user) {
            throw ApiError.AuthorizeRequired();
        }

        const { userId } = req.user;
        const { docId } = req.body;

        if (!docId) {
            throw ApiError.BadRequest('doc id is required');
        }

        const doc = await DocumentModel.findOne({ userId, docId });

        if (!doc) {
            throw ApiError.NotFound('doc not found');
        }

        res.send({ doc: clientifyDoc(doc) });
    };

    getUserDocuments = async (req: FastifyRequest, res: FastifyReply) => {
        if (!req.user) {
            throw ApiError.AuthorizeRequired();
        }

        const { userId } = req.user;
        const documents = await DocumentModel.find({ userId });

        res.send({ docs: documents.map(getShortDocumentInfo) });
    };

    updateDocumentSnapshot = (
        req: FastifyRequest<{ Body: { snapshot: string } }>,
        res: FastifyReply,
    ) => {
        const { snapshot } = req.body;

        if (!snapshot) throw ApiError.BadRequest('snapshot is required');

        res.send(snapshot);
    };

    getDocumentContent = async (
        req: FastifyRequest<{ Querystring: { docId: string; key: string } }>,
        res: FastifyReply,
    ) => {
        const { docId, key } = req.query;

        if (!docId || !key)
            throw ApiError.BadRequest('docID and key are required');

        const canAccessDocument = documentsService.checkDocumentAccess(
            docId,
            key,
        );

        if (!canAccessDocument) {
            throw ApiError.ForbiddenError();
        }

        const documentContent =
            await documentsService.docPersistence.getDocument(docId);

        res.send({ content: documentContent });
    };
}
