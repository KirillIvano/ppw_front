import { UserData } from '@ppw/shared/types/user';

declare module 'fastify' {
    interface FastifyRequest {
        user: UserData;
    }
}
