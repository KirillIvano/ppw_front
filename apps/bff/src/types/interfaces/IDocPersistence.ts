export type IDocPersistence = {
    saveDocument: (name: string, file: string) => Promise<void>;
    getDocument: (name: string) => Promise<string>;
};
