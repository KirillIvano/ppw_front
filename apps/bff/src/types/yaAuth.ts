export type YaAuthData = {
    id: string;
    login: string;
    default_email: string;
};

export type YaPreparedInfo = {
    userId: string;
    login: string;
};
