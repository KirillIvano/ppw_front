import { DEBUG_MODE } from '@bff/settings';
import { UserData } from '@ppw/shared/types/user';
import { FastifyInstance } from 'fastify';

import { ApiError } from '../exceptions/api-error';
import { tokenService } from '../service/token-service';

const DEGUB_USER: UserData = {
    userId: '1012858596',
    login: 'wgwergwegwer.work',
};

export const registerAuthMiddleware = (app: FastifyInstance) => {
    app.addHook('onRequest', (req, res, done) => {
        try {
            /** Для тестирования */
            if (DEBUG_MODE) {
                req.user = DEGUB_USER;

                return done();
            }

            const authorizationHeader = req.headers.authorization;
            if (!authorizationHeader) {
                return done(ApiError.UnauthorizedError());
            }

            const accessToken = authorizationHeader.split(' ')[1];
            if (!accessToken) {
                return done(ApiError.UnauthorizedError());
            }

            const userData = tokenService.validateAccessToken(accessToken);
            if (!userData) {
                return done(ApiError.UnauthorizedError());
            }

            req.user = userData;
            done();
        } catch {
            return done(ApiError.UnauthorizedError());
        }
    });
};
