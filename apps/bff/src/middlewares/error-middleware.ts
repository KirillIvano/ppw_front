import { FastifyInstance } from 'fastify';

import { ApiError } from '../exceptions/api-error';

export const registerErrorHandler = (app: FastifyInstance) => {
    app.setErrorHandler(async (err, req, res) => {
        app.log.error(err);

        if (err instanceof ApiError) {
            return res.status(err.status).send({ error: err.message });
        }

        return res.status(500).send({ error: 'Непредвиденная ошибка' });
    });
};
