import WebSocket from 'ws';
import http from 'http';

const wss = new WebSocket.Server({ noServer: true });
import { setupWSConnection } from './utils';
import { DocParams } from './types';

export const initDocsServer = (serverParams: DocParams) => {
    const server = http.createServer((_, response) => {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('okay');
    });

    wss.on('connection', (conn, req) =>
        setupWSConnection(conn, req, serverParams),
    );

    server.on('upgrade', (request, socket, head) => {
        const handleAuth = (ws: WebSocket) => {
            wss.emit('connection', ws, request);
        };

        wss.handleUpgrade(request, socket, head, handleAuth);
    });

    server.listen({ port: serverParams.port, host: serverParams.host }, () =>
        serverParams.logger.info(
            `Socket server started on ${serverParams.port}`,
        ),
    );
};
