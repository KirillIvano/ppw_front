import { Logger } from 'pino';
import { WSSharedDoc } from './WSSharedDoc';

export type DocParams = {
    /** Инстанс логгера */
    logger: Logger;
    /** Порт, на котором должно быть запущено приложение */
    port: number;
    /** Хост, на котором нужно запустить приложение */
    host: string;
    /** Время, которое должно пройти между обновлениями для обновления состояния документа */
    persistanceDebounce: number;
    /** Максимальное время между сохранениями состояния документа */
    persistanceMaxWait: number;

    /** Обработчик, вызываемый при сохранении документа */
    onPersist: (doc: WSSharedDoc) => Promise<void>;
    /** Обработчик, вызываемый при создании объекта документа */
    onInit: (doc: WSSharedDoc) => Promise<void>;
    /** Обработчик, вызываемый, когда у документа не остается слушателей */
    onIdle: (doc: WSSharedDoc) => Promise<void>;
};
