import pino from 'pino';

import { documentsService } from '@bff/service/document-service';

import { docs } from './docsRepo';
import { initDocsServer } from './server';
import { WSSharedDoc } from './WSSharedDoc';

const persist = async (doc: WSSharedDoc) => {
    const quillDoc = doc.getText('quill');
    const quillDelta = quillDoc.toDelta();

    const eventualText = doc.getText('quill_persisted');

    // fully replace old content
    eventualText.applyDelta([
        ...(eventualText.toDelta().length
            ? [{ delete: eventualText.length }]
            : []),
        ...quillDelta,
    ]);

    await documentsService.docPersistence.saveDocument(
        doc.name,
        JSON.stringify(quillDelta),
    );
};

const idle = async (doc: WSSharedDoc) => {
    docs.delete(doc);
};

const init = async (doc: WSSharedDoc) => {
    const deltaStr = await documentsService.docPersistence.getDocument(
        doc.name,
    );

    const delta = deltaStr ? (JSON.parse(deltaStr) as any[]) : [];

    const quillText = doc.getText('quill');
    const eventualQuillText = doc.getText('quill_persisted');

    doc.transact(() => {
        quillText.applyDelta(delta);
        eventualQuillText.applyDelta(delta);
    });
};

const logger = pino();

export const startYjsSocket = () =>
    initDocsServer({
        logger,
        port: 1234,
        host: 'localhost',
        persistanceMaxWait: 30000,
        persistanceDebounce: 3000,
        onInit: init,
        onPersist: persist,
        onIdle: idle,
    });
