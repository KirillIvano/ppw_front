import * as Y from 'yjs';

import { WebSocket } from 'ws';

import * as syncProtocol from 'y-protocols/sync';
import * as awarenessProtocol from 'y-protocols/awareness';
import * as encoding from 'lib0/encoding';
import * as decoding from 'lib0/decoding';

import { DocParams } from './types';

import { debounce } from 'lodash';
import { messageAwareness, messageSync } from './constants';

const gcEnabled = process.env.GC !== 'false' && process.env.GC !== '0';

export class WSSharedDoc extends Y.Doc {
    conns: Map<WebSocket, Set<number>> = new Map();
    awareness: awarenessProtocol.Awareness = new awarenessProtocol.Awareness(
        this,
    );

    constructor(public name: string, private docParams: DocParams) {
        super({ gc: gcEnabled });

        this.awareness.setLocalState(null);

        this.awareness.on('update', this.handleAwarenessChange);
        this.on('update', this.handleDocChange);

        this.on(
            'update',
            debounce(
                () =>
                    this.docParams
                        .onPersist(this)
                        .catch((e) => this.docParams.logger.error(e)),
                docParams.persistanceDebounce,
                {
                    maxWait: docParams.persistanceMaxWait,
                },
            ),
        );
    }

    init = async () => {
        await this.docParams
            .onInit(this)
            .catch((e) => this.docParams.logger.error(e));
    };

    attachConnection = (conn: WebSocket) => {
        this.conns.set(conn, new Set());

        this.initConnectionData(conn);

        conn.on('message', (message) =>
            this.handleMessage(conn, new Uint8Array(message as ArrayBuffer)),
        );

        conn.on('close', () => {
            this.closeConn(conn);
            clearInterval(pingInterval);
        });

        conn.on('pong', () => {
            pongReceived = true;
        });

        let pongReceived = true;
        const pingInterval = setInterval(() => {
            if (!pongReceived) {
                if (this.conns.has(conn)) {
                    this.closeConn(conn);
                }
                clearInterval(pingInterval);
            } else if (this.conns.has(conn)) {
                pongReceived = false;

                try {
                    conn.ping();
                } catch (e) {
                    this.closeConn(conn);
                    clearInterval(pingInterval);
                }
            }
        }, 30000);
    };

    /** gracefully closes connection */
    private closeConn = (conn: WebSocket) => {
        if (this.conns.has(conn)) {
            const controlledIds = this.conns.get(conn) as Set<number>;

            this.conns.delete(conn);
            awarenessProtocol.removeAwarenessStates(
                this.awareness,
                Array.from(controlledIds),
                null,
            );

            // close doc if no users left
            if (this.conns.size === 0) {
                this.docParams
                    .onPersist(this)
                    .catch((e) => this.docParams.logger.error(e));
                this.docParams
                    .onIdle(this)
                    .catch((e) => this.docParams.logger.error(e));
            }
        }
        conn.close();
    };

    /** sends data to connection */
    private send = (conn: WebSocket, m: Uint8Array) => {
        if (
            conn.readyState !== WebSocket.CONNECTING &&
            conn.readyState !== WebSocket.OPEN
        ) {
            this.closeConn(conn);
        }

        try {
            conn.send(m, (err) => {
                err != null && this.closeConn(conn);
            });
        } catch (e) {
            this.closeConn(conn);
        }
    };

    /** broadcasts changes to all document connections  */
    private handleDocChange = (update: Uint8Array) => {
        const encoder = encoding.createEncoder();

        encoding.writeVarUint(encoder, messageSync);
        syncProtocol.writeUpdate(encoder, update);

        const message = encoding.toUint8Array(encoder);

        this.conns.forEach((_, conn) => this.send(conn, message));
    };

    /** broadcasts awareness updates to all users  */
    private handleAwarenessChange = (
        {
            added,
            updated,
            removed,
        }: {
            added: number[];
            updated: number[];
            removed: number[];
        },
        conn: WebSocket,
    ) => {
        const changedClients = added.concat(updated, removed);

        if (conn !== null) {
            const connControlledIDs = this.conns.get(conn);

            if (connControlledIDs !== undefined) {
                added.forEach((clientID: number) => {
                    connControlledIDs.add(clientID);
                });
                removed.forEach((clientID: number) => {
                    connControlledIDs.delete(clientID);
                });
            }
        }

        const encoder = encoding.createEncoder();
        encoding.writeVarUint(encoder, messageAwareness);
        encoding.writeVarUint8Array(
            encoder,
            awarenessProtocol.encodeAwarenessUpdate(
                this.awareness,
                changedClients,
            ),
        );

        const buff = encoding.toUint8Array(encoder);
        this.conns.forEach((_, connection) => {
            this.send(connection, buff);
        });
    };

    /** updates document by outer message */
    private handleMessage = (conn: WebSocket, message: Uint8Array) => {
        try {
            const encoder = encoding.createEncoder();
            const decoder = decoding.createDecoder(message);
            const messageType = decoding.readVarUint(decoder);

            switch (messageType) {
                case messageSync:
                    encoding.writeVarUint(encoder, messageSync);
                    syncProtocol.readSyncMessage(decoder, encoder, this, null);
                    if (encoding.length(encoder) > 1) {
                        this.send(conn, encoding.toUint8Array(encoder));
                    }
                    break;
                case messageAwareness: {
                    awarenessProtocol.applyAwarenessUpdate(
                        this.awareness,
                        decoding.readVarUint8Array(decoder),
                        conn,
                    );
                    break;
                }
            }
        } catch (err) {
            this.emit('error', [err]);
        }
    };

    /** synchronize local document with remote */
    private initConnectionData = (conn: WebSocket) => {
        const encoder = encoding.createEncoder();

        encoding.writeVarUint(encoder, messageSync);
        syncProtocol.writeSyncStep1(encoder, this);

        this.send(conn, encoding.toUint8Array(encoder));

        const awarenessStates = this.awareness.getStates();

        if (awarenessStates.size > 0) {
            const encoder = encoding.createEncoder();

            encoding.writeVarUint(encoder, messageAwareness);
            encoding.writeVarUint8Array(
                encoder,
                awarenessProtocol.encodeAwarenessUpdate(
                    this.awareness,
                    Array.from(awarenessStates.keys()),
                ),
            );

            this.send(conn, encoding.toUint8Array(encoder));
        }
    };
}
