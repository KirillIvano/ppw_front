import type { IncomingMessage } from 'http';
import { WebSocket } from 'ws';

import { DocParams } from './types';
import { WSSharedDoc } from './WSSharedDoc';
import { docs } from './docsRepo';

export const getDocContent = (doc: WSSharedDoc, name: string) => {
    const docName = doc.name;
    const text = doc.getText(name);

    return { docName, content: JSON.stringify(text.toDelta()) };
};

export const getYDoc = async (
    docname: string,
    serverParams: DocParams,
): Promise<WSSharedDoc> => {
    if (docs.has(docname)) return docs.get(docname);

    const doc = new WSSharedDoc(docname, serverParams);

    await doc.init();
    docs.set(docname, doc);

    return doc;
};

export const setupWSConnection = async (
    conn: WebSocket,
    req: IncomingMessage,
    serverParams: DocParams,
) => {
    conn.binaryType = 'arraybuffer';

    const docName = (req.url as string).slice(1).split('?')[0];
    const doc = await getYDoc(docName, serverParams);

    doc.attachConnection(conn);
};
