import fetch from 'node-fetch';
import qs from 'qs';

import type { YaAuthData, YaPreparedInfo } from '@bff/types/yaAuth';
import type { UserData } from '@ppw/shared/types/user';

import { clientifyUserData, UserModel } from '../database/models/user-model';
import { tokenService } from './token-service';
import { ApiError } from '../exceptions/api-error';

class UserService {
    private async verifyToken(token: string): Promise<YaPreparedInfo> {
        const res = await fetch(
            `https://login.yandex.ru/info?${qs.stringify({ format: 'json' })}`,
            {
                headers: { Authorization: `OAuth ${token}` },
            },
        );
        if (!res.ok) throw ApiError.BadRequest('failed to authorize token');

        const data = (await res.json()) as YaAuthData;

        return { userId: data.id, login: data.login };
    }

    private getUserFromYaInfo = async (
        info: YaPreparedInfo,
    ): Promise<UserData> => {
        const user = await UserModel.findOne({ userId: info.userId });

        if (user) return clientifyUserData(user);

        const createdUser = await UserModel.create({
            userId: info.userId,
            login: info.login,
        });
        return clientifyUserData(createdUser);
    };

    async authorize(token: string) {
        const yaInfo = await this.verifyToken(token);
        const userDto = await this.getUserFromYaInfo(yaInfo);

        const tokens = tokenService.generateTokens({ ...userDto });
        await tokenService.saveToken(userDto.userId, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }

    async logout(refreshToken: string) {
        const token = await tokenService.removeToken(refreshToken);
        return token;
    }

    async refresh(refreshToken: string) {
        if (!refreshToken) {
            throw ApiError.UnauthorizedError();
        }
        const userData = tokenService.validateRefreshToken(
            refreshToken,
        ) as UserData;
        const tokenFromDb = await tokenService.findToken(refreshToken);
        if (!userData || !tokenFromDb) {
            throw ApiError.UnauthorizedError();
        }
        const user = await UserModel.findById(userData.userId);

        if (!user) {
            throw ApiError.UnauthorizedError();
        }

        const userDto = clientifyUserData(user);
        const tokens = tokenService.generateTokens({ ...userDto });

        await tokenService.saveToken(userDto.userId, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }

    async getAllUsers() {
        const users = await UserModel.find();
        return users;
    }
}

export const userService = new UserService();
