import jwt from 'jsonwebtoken';

import { JWT_ACCESS_SECRET, JWT_REFRESH_SECRET } from '@bff/settings';
import { UserData } from '@ppw/shared/types/user';

import { TokenModel } from '../database/models/token-model';

class TokenService {
    /** 15 days */
    readonly REFRESH_EXPIERY = 30 * 24 * 3600;
    /** 15 minutes */
    readonly ACCESS_EXPIERY = 15 * 3600;

    generateTokens(payload: UserData) {
        const accessToken = jwt.sign(payload, JWT_ACCESS_SECRET, {
            expiresIn: this.ACCESS_EXPIERY,
        });
        const refreshToken = jwt.sign(payload, JWT_REFRESH_SECRET, {
            expiresIn: this.REFRESH_EXPIERY,
        });
        return {
            accessToken,
            refreshToken,
        };
    }

    validateAccessToken(token: string) {
        try {
            const userData = jwt.verify(token, JWT_ACCESS_SECRET);
            return userData as UserData;
        } catch (e) {
            return null;
        }
    }

    validateRefreshToken(token: string) {
        try {
            const userData = jwt.verify(token, JWT_REFRESH_SECRET);
            return userData as UserData;
        } catch (e) {
            return null;
        }
    }

    async saveToken(userId: string, refreshToken: string) {
        const tokenData = await TokenModel.findOne({ user: userId });
        if (tokenData) {
            tokenData.refreshToken = refreshToken;
            return tokenData.save();
        }
        const token = await TokenModel.create({ user: userId, refreshToken });
        return token;
    }

    async removeToken(refreshToken: string) {
        const tokenData = await TokenModel.deleteOne({ refreshToken });
        return tokenData;
    }

    async findToken(refreshToken: string) {
        const tokenData = await TokenModel.findOne({ refreshToken });
        return tokenData;
    }
}

export const tokenService = new TokenService();
