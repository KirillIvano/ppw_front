import fs from 'fs/promises';
import path from 'path';
import { always } from 'ramda';

import { DOCUMENTS_STORAGE } from '@bff/settings';
import { IDocPersistence } from '@bff/types/interfaces/IDocPersistence';

const DEFAULT_DOC = '[]';

export const docPersistence: IDocPersistence = {
    saveDocument: async (name: string, file: string): Promise<void> => {
        await fs.writeFile(path.join(DOCUMENTS_STORAGE, name), file);
    },
    getDocument: async (name: string) => {
        const documentPath = path.join(DOCUMENTS_STORAGE, name);

        return fs
            .readFile(documentPath)
            .then((file) => file.toString())
            .catch(always(DEFAULT_DOC));
    },
};
