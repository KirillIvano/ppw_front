import {
    DocumentAccessModel,
    DocumentModel,
    getDocumentAccessData,
} from '@bff/database/models/document-model';
import { generateId } from '@bff/utils/generateId';
import { IDocPersistence } from '@bff/types/interfaces/IDocPersistence';
import { docPersistence } from './doc-persistance';
import { DocumentAccessData } from '@ppw/shared/types/document';

class DocumentsService {
    constructor(public docPersistence: IDocPersistence) {}

    generateDocId = async () => {
        let docId = generateId();

        while (await DocumentModel.exists({ docId })) {
            docId = generateId();
        }

        return docId;
    };

    checkDocumentAccess = (docId: string, key: string): Promise<boolean> => {
        return DocumentAccessModel.exists({ docId, key }).then(Boolean);
    };

    grantReadonlyAccess = async (
        docId: string,
    ): Promise<DocumentAccessData> => {
        const key = generateId();

        const createdDocument = await DocumentAccessModel.create({
            docId,
            key,
        });

        return getDocumentAccessData(createdDocument);
    };
}

export const documentsService = new DocumentsService(docPersistence);
