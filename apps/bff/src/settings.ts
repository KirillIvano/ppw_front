import { omitUndefined } from '@ppw/shared';

export const MONGO_PASSWORD = omitUndefined(process.env.MONGO_PASSWORD);
export const MONGO_USER = omitUndefined(process.env.MONGO_USER);
export const MONGO_HOST = omitUndefined(process.env.MONGO_HOST);
export const MONGO_PORT = omitUndefined(process.env.MONGO_PORT);
export const MONGO_DATABASE = omitUndefined(process.env.MONGO_DATABASE);
export const PORT = process.env.PORT ? +process.env.PORT : 5001;
export const CLIENT_URL = omitUndefined(process.env.CLIENT_URL);
export const JWT_REFRESH_SECRET = omitUndefined(process.env.JWT_REFRESH_SECRET);
export const JWT_ACCESS_SECRET = omitUndefined(process.env.JWT_ACCESS_SECRET);
export const DEBUG_MODE = Boolean(process.env.DEBUG);
export const DOCUMENTS_STORAGE = omitUndefined(process.env.DOCUMENTS_STORAGE);
