import {
    MONGO_HOST,
    MONGO_PASSWORD,
    MONGO_PORT,
    MONGO_USER,
} from './../settings';
import mongoose from 'mongoose';

export const getMongoConnectionString = () =>
    `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}`;

export const connect = async () => {
    const connectionString = getMongoConnectionString();

    await mongoose.connect(connectionString);
};
