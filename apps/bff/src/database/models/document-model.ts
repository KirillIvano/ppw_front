import { Schema, model, type Document as MongooseDocument } from 'mongoose';
import { pick } from 'ramda';

import {
    DocumentAccessData,
    DocumentData,
    ShortDocumentInfo,
} from '@ppw/shared/types/document';

export const clientifyDoc: (
    doc: DocumentData & MongooseDocument,
) => DocumentData = pick(['documentSnapshot', 'docId', 'userId', 'name']);

export const getShortDocumentInfo: (doc: DocumentData) => ShortDocumentInfo =
    pick(['docId', 'userId', 'name']);

export const getDocumentAccessData: (
    doc: DocumentAccessData & DocumentAccessData,
) => DocumentAccessData = pick(['docId', 'key']);

const DocumentSchema = new Schema({
    documentSnapshot: { type: String, default: '' },
    name: { type: String, required: true },
    docId: { type: String, required: true, unique: true },
    userId: { type: String, required: true },
});

const DocumentAccess = new Schema({
    docId: { type: String, required: true },
    key: { type: String, required: true },
});

export const DocumentAccessModel = model<DocumentAccessData & MongooseDocument>(
    'DocumentAccess',
    DocumentAccess,
);

export const DocumentModel = model<DocumentData & MongooseDocument>(
    'Document',
    DocumentSchema,
);
