import { Schema, model, type Document as MongooseDocument } from 'mongoose';
import { UserData } from '@ppw/shared/types/user';

export const clientifyUserData = (
    user: UserData & MongooseDocument,
): UserData => ({
    userId: user.id,
    login: user.login,
});

const UserSchema = new Schema({
    userId: { type: String, unique: true, required: true },
    login: { type: String, unique: true, required: true },
});

export const UserModel = model<UserData & MongooseDocument>('User', UserSchema);
