import { Schema, model, type Document as MongooseDocument } from 'mongoose';

export type TokenData = {
    user: string;
    refreshToken: string;
};

const TokenSchema = new Schema({
    user: { type: String, required: true },
    refreshToken: { type: String, required: true },
});

export const TokenModel = model<TokenData & MongooseDocument>(
    'Token',
    TokenSchema,
);
