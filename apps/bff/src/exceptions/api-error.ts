export class ApiError extends Error {
    constructor(
        public readonly status: number,
        public readonly message: string,
    ) {
        super(message);
    }

    static UnauthorizedError() {
        return new ApiError(401, 'Пользователь не авторизован');
    }

    static AuthorizeRequired() {
        return new ApiError(500, 'Контроллер требует авторизации');
    }

    static ForbiddenError() {
        return new ApiError(403, 'Нет доступа');
    }

    static NotFound(message: string) {
        return new ApiError(404, message);
    }

    static BadRequest(message: string) {
        return new ApiError(400, message);
    }
}
