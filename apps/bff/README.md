Мини-сервис для загрузки и сохранения изображений

Необходимые энвы:
* STORAGE_PATH - абсолютный путь к хранилищу изображений на сервере
* PORT - порт на котором повиснет сервер


TO IMPROVE:
* авторизация изображений, сейчас они будут отдаваться всем пользователям через nginx, что не очень хорошо, плюс нужна защита от ddos, если кто-то будет загружать слишком много пикч
* развертывание в общем докере и в дев, и в продакшн среде
