import { omitUndefined } from './utils';

export const IMAGES_PATH = omitUndefined<string>(process.env.STORAGE_PATH);
export const PORT = process.env.PORT ?? 3001;
