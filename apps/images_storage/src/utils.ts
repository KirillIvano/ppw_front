import { randomBytes } from 'crypto';

export function omitUndefined<T>(val: T | undefined): T {
    if (val === undefined) throw new Error();

    return val;
}

export const getRandomHash = () => randomBytes(16).toString('hex');
