import fs from 'fs/promises';
import mime from 'mime';
import path from 'path';

import { MultipartFile } from '@fastify/multipart';

import { getRandomHash } from './utils';

export class ImagesService {
    // кэшируем текущие файлы, чтобы не ходить каждый раз на сервер
    currentImages: Set<string> = new Set();

    constructor(private readonly imagesPath: string) {}

    async initService() {
        const files = await fs.readdir(this.imagesPath);
        const filesIds = this.getIdsFromFiles(files);

        this.currentImages = new Set(filesIds);
    }

    private getAvailableId = () => {
        let hash = getRandomHash();

        while (this.currentImages.has(hash)) {
            hash = getRandomHash();
        }

        return hash;
    };

    async uploadImage(data: MultipartFile): Promise<string | null> {
        const { file, mimetype } = data;

        const imageId = this.getAvailableId();
        const extension = mime.extension(mimetype);

        if (!extension) return null;

        const fullFileName = `${imageId}.${extension}`;

        try {
            await fs.writeFile(path.join(this.imagesPath, fullFileName), file);
        } catch (e) {
            return null;
        }

        return fullFileName;
    }

    private getIdsFromFiles(files: string[]) {
        return files.map((file) => file.split('.')[0]);
    }
}
