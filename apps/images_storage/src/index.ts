import 'dotenv/config';

import fastify from 'fastify';

import cors from '@fastify/cors';
import multipart from '@fastify/multipart';
import staticServe from '@fastify/static';

import { IMAGES_PATH, PORT } from './settings';
import { ImagesService } from './ImagesService';

const app = fastify();

app.register(cors);
app.register(multipart);

const imagesService = new ImagesService(IMAGES_PATH);

app.register(staticServe, { root: IMAGES_PATH, prefix: '/image/' });

app.post('/upload', async (req, res) => {
    if (!req.isMultipart()) return { error: 'Файл не является изображением' };

    try {
        const fileData = await req.file();
        const uploadResult = await imagesService.uploadImage(fileData);

        if (uploadResult === null)
            return res.status(400).send('Failed to save image');

        res.send({ image: uploadResult });
    } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
        res.status(400).send({ error: 'Bad Request' });
    }
});

app.listen(PORT);
